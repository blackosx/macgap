#MacGap

This was forked from [maccman/macgap](https://github.com/maccman/macgap), which itself was forked from [Phonegap-mac](https://github.com/shazron/phonegap-mac)

Full information of the project can be read at [maccman's github repository](https://github.com/maccman/macgap)

#About MacGap

The MacGap project aims to provide HTML/JS/CSS developers an Xcode project for developing Native OSX Apps that run in OSX's WebView and take advantage of WebKit technologies. The project also exposes a basic JavaScript API for OS integration, such as display Growl notifications. The MacGap project is extremely lightweight and nimble, a blank application is about 0.3mb.

#Why have i forked it?

I have used MacGap for the user interface in [DarwinDumper](https://bitbucket.org/blackosx/darwindumper).
To suit this purpose, I made a couple of basic changes and may make more in the future so I thought it best to have a repo to show the workings.
I will document the changes I make in a Changes.txt file.

#License

[MIT License (MIT)](http://opensource.org/licenses/MIT)
