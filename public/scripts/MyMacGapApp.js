// A script template for using with MacGap apps
// Copyright (C) 2014-2015 Blackosx
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//Version=0.0.0

var gTmpDir = "/tmp/MyMacGapApp";
var gLogBashToJs = "bashToJs";

//-------------------------------------------------------------------------------------
// On initial load
$(document).ready(function() {
    readBashToJsMessageFile();
});

//-------------------------------------------------------------------------------------
// Called when the process is to close.
function terminate() {
    clearTimeout(timerReadMessageFile);
    macgap.notice.close("*"); // Remove all notifications sent by app
    macgap.app.terminate();    
}

//-------------------------------------------------------------------------------------
// looks for a file and if found, returns the contents
function GetFileContents(filename)
{
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET",gTmpDir+"/"+filename,false);
    xmlhttp.send(null);

    fileContent = xmlhttp.responseText;

    if (fileContent != "" ) {
        return fileContent;
    } else {
        return 0;
    }
}

//-------------------------------------------------------------------------------------
// Check for incoming messages from bash script
function readBashToJsMessageFile()
{
    incoming=GetFileContents(gLogBashToJs);
    if (incoming != 0) {
    
        // Split settings by newline
        var incoming = incoming.split('\n');
        
        // Take first line
        var firstLine = (incoming[0]);
    
        // Split firstLine by @
        var firstLineSplit = (firstLine).split('@');
        var firstLineCommand = (firstLineSplit[0]);
        
        // match command against known ones.
        switch(firstLineCommand) {
            case "Welcome":
                // Bash sends: "Welcome@@"
                macgap.app.removeMessage(firstLine);
                showWelcomeMessage(); 
                break;
            case "Snow":
                // Bash sends: "Snow@${gSnow}@"
                macgap.app.removeMessage(firstLine);
                ToggleSnow(firstLineSplit[1]);
                break;
            default:
                alert("Found else:" + firstLine + " | This is a problem and the app may not function correctly. Please report this error");
                if(firstLine == "") {
                    macgap.app.removeMessage("");
                } else {
                    macgap.app.removeMessage(firstLine);
                }
                break;
        }
	    // recursively call function as long as file exists every 1/10th second.
        timerReadMessageFile = setTimeout(readBashToJsMessageFile, 100);
    } else {
        // recursively call function as long as file exists but at 1/2 second intervals
        timerReadMessageFile = setTimeout(readBashToJsMessageFile, 500);
    }
}

//-------------------------------------------------------------------------------------
function showWelcomeMessage()
{
    alert("Welcome");
}        

//-------------------------------------------------------------------------------------
$(function()
{
    //-----------------------------------------------------
    // On clicking the Snow button
    $("#SnowToggleButton").on('click', function() {
        var textState = $(this).text();
        if (textState.indexOf("Snow Off") >= 0) {
            ToggleSnow("Off");
            macgap.app.launch("CTM_Snow@Off");
        } else {  
            ToggleSnow("On");
            macgap.app.launch("CTM_Snow@On");
        }
    });
});

//-------------------------------------------------------------------------------------
function ToggleSnow(action)
{
    if (action == "Off") {
        snowStorm.stop();
        $("#SnowToggleButton").text("Snow On");
    } else if (action == "On") {  
        snowStorm.resume();
        $("#SnowToggleButton").text("Snow Off");
    }
}
