#!/bin/bash

# Resolve path
#SELF_PATH=$(cd -P -- "$(dirname -- "$0")" && pwd -P) && SELF_PATH=$SELF_PATH/$(basename -- "$0")

# Set out other directory paths based on SELF_PATH
PUBLIC_DIR="${SELF_PATH%/*}"
PUBLIC_DIR="${PUBLIC_DIR%/*}"
ASSETS_DIR="$PUBLIC_DIR"/assets
SCRIPTS_DIR="$PUBLIC_DIR"/bash
JSSCRIPTS_DIR="$PUBLIC_DIR"/scripts
TOOLS_DIR="$PUBLIC_DIR"/tools
WORKING_PATH="${HOME}/Library/Application Support"
APP_DIR_NAME="MyMacGapApp"
TEMPDIR="/tmp/${APP_DIR_NAME}"

# Set out file paths
logFile="${TEMPDIR}/${APP_DIR_NAME}_Log.txt"
logJsToBash="${TEMPDIR}/jsToBash" # Note - this is created in AppDelegate.m
logBashToJs="${TEMPDIR}/bashToJs" # Note - this is created in AppDelegate.m

# Globals
debugIndent="    "
debugIndentTwo="${debugIndent}${debugIndent}"
COMMANDLINE=0
DEBUG=1

# Common Functions
# ---------------------------------------------------------------------------------------
WriteToLog() {
    if [ $COMMANDLINE -eq 0 ]; then
        printf "${1}\n" >> "$logFile"
    else
        printf "${1}\n"
    fi
}

# ---------------------------------------------------------------------------------------
WriteLinesToLog() {
    if [ $COMMANDLINE -eq 0 ]; then
        if [ $DEBUG -eq 1 ]; then
            printf "${debugIndent}===================================\n" >> "$logFile"
        else
            printf "===================================\n" >> "$logFile"
        fi
    else
        printf "===================================\n"
    fi
}

# ---------------------------------------------------------------------------------------
SendToUI() {
    echo "${1}" >> "$logBashToJs"
}