#!/bin/bash

# A script showing Empty Framework
# Copyright (C) 2014-2015 Blackosx
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

VERS="0.0.0"

# =======================================================================================
# Helper Functions/Routines
# =======================================================================================

# ---------------------------------------------------------------------------------------
RemoveFile()
{
    if [ -f "$1" ]; then
        rm "$1"
    fi
}

# ---------------------------------------------------------------------------------------
ClearTopOfMessageLog()
{
    # removes the first line of the log file.
    local log=$(tail -n +2 "$1"); > "$1" && if [ "$log" != "" ]; then echo "$log" > "$1"; fi
}


# =======================================================================================
# After Initialisation Routines
# =======================================================================================

# ---------------------------------------------------------------------------------------
RespondToUserSnowToggle()
{
    [[ DEBUG -eq 1 ]] && WriteLinesToLog
    [[ DEBUG -eq 1 ]] && WriteToLog "${debugIndent}RespondToUserSnowToggle()"
    
    local messageFromUi="$1"

    # remove everything up until, and including, the first @
    messageFromUi="${messageFromUi#*@}"
    chosenOption="${messageFromUi##*:}"

    if [ ! "$chosenOption" == "" ]; then
        if [ "$chosenOption" == "On" ]; then
            [[ DEBUG -eq 1 ]] && WriteToLog "${debugIndentTwo}User chose to set enable Snow."
            gSnow="On"
        elif [ "$chosenOption" == "Off" ]; then
            [[ DEBUG -eq 1 ]] && WriteToLog "${debugIndentTwo}User chose to set disable Snow."
            gSnow="Off"
        fi
    fi
}


# ---------------------------------------------------------------------------------------
CleanUp()
{
    [[ DEBUG -eq 1 ]] && WriteLinesToLog
    [[ DEBUG -eq 1 ]] && WriteToLog "${debugIndent}CleanUp()"
    
    RemoveFile "$logJsToBash"
    RemoveFile "$logFile"
    RemoveFile "$logBashToJs"2
    
    # Remove symbolic link for the scripts files
    if [ -h "${TEMPDIR}"/scripts/jquery-2.1.3.min.js ]; then
        rm "${TEMPDIR}"/scripts/jquery-2.1.3.min.js
    fi

    # Remove symbolic link for the styles dir
    if [ -h "${TEMPDIR}"/styles ]; then
        rm "${TEMPDIR}"/styles
    fi
    
    # Remove symbolic link for the assets dir
    if [ -h "${TEMPDIR}"/assets ]; then
        rm "${TEMPDIR}"/assets
    fi
    
    #if [ -d "$tmp_dir" ]; then
    #    rm -rf "$tmp_dir"
    #fi
    
    # Remove the copy of cloverthememanager.js
    if [ -f "${TEMPDIR}"/scripts/MyMacGapApp.js ]; then
        rm "${TEMPDIR}"/scripts/MyMacGapApp.js
    fi

    # Remove the temporary scripts dir
    if [ -d "${TEMPDIR}"/scripts ]; then
        rmdir "${TEMPDIR}"/scripts
    fi
    
    # Remove the temporary dir
    if [ -d "${TEMPDIR}" ]; then
        rmdir "${TEMPDIR}"
    fi
    
    # Check again and force if not removed
    if [ -d "${TEMPDIR}" ]; then
        rm -rf "${TEMPDIR}"
    fi
}

#===============================================================
# Main
#===============================================================

# Note: This script needs to exit when the parent app is closed.

scriptPid=$( echo "$$" )                          # Get process ID of this script
appPid=$( ps -p ${pid:-$$} -o ppid= )             # Get process ID of parent

# Resolve path
SELF_PATH=$(cd -P -- "$(dirname -- "$0")" && pwd -P) && SELF_PATH=$SELF_PATH/$(basename -- "$0")
source "${SELF_PATH%/*}"/shared.sh

# Globals
gitCmd=""                                         # Will be set to path to installed git binary to use
gSnow="Off"                                       # Default state for Snow effect. ** Should really remove this! **

# Find version of main app.
mainAppInfoFilePath="${SELF_PATH%Resources*}"
mainAppVersion=$( /usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$mainAppInfoFilePath"/Info.plist  )

# Begin log file
RemoveFile "$logFile"
WriteToLog "MyMacGapApp ${mainAppVersion}"
WriteToLog "Started MyMacGapApp script"
WriteLinesToLog
WriteToLog "scriptPid=$scriptPid | appPid=$appPid"
WriteLinesToLog
[[ DEBUG -eq 1 ]] && WriteToLog "${debugIndent}PATH=$PATH"
WriteToLog "==== Initialisation Start ===="

# Was this script called from a script or the command line
identityCallerCheck=`ps -o stat= -p $$`
if [ "${identityCallerCheck:1:1}" == "+" ]; then
    # Called from command line so interpret arguments.

    if [ "$#" -eq 2 ]; then
        echo "Welcome to MyMacGapApp. You passed $0."
        echo "Exiting."
    fi

    # Redirect all log file output to stdout
    COMMANDLINE=1    

else

    # Copy cloverthememanager.js to temp dir
    mkdir "$TEMPDIR"/scripts
    cp "$JSSCRIPTS_DIR"/MyMacGapApp.js "$TEMPDIR"/scripts

    # Feedback for command line
    echo "Initialisation complete. Entering loop."

    # Remember parent process id
    parentId=$appPid

    [[ DEBUG -eq 1 ]] && WriteToLog "${debugIndentTwo}Sending UI: Welcome@@"
    SendToUI "Welcome@@"

    #===============================================================
    # Main Message Loop for responding to UI feedback
    #===============================================================
        
    # The messaging system is event driven and quite simple.
    # Run a loop for as long as the parent process ID still exists
    while [ "$appPid" == "$parentId" ];
    do
        sleep 0.25                                # Check every 1/4 second.
        logLine=$(head -n 1 "$logJsToBash")       # Read first line of log file


        #===============================================================
        # Add any possible strings below that could be sent from javascript
        #===============================================================

        if [[ "$logLine" == *CTM_Snow* ]]; then   # User clicked Snow toggle button
            ClearTopOfMessageLog "$logJsToBash"
            RespondToUserSnowToggle "$logLine"
        fi


        appPid=$( ps -p ${pid:-$$} -o ppid= )     # Get process ID of parent
    done
    
    CleanUp    
    exit 0
fi
