// Application constants

#define kStartPage @"public/index.html"

#define kStartFolder @"."

#define kWebScriptNamespace @"macgap"

extern NSString *MY_APP_NAME;
extern NSString *TMP_DIR;
extern NSString *BASH_TO_JS_FILENAME;
extern NSString *JS_TO_BASH_FILENAME;
extern NSString *TMP_APP_DIR;

extern NSString *BASH_TO_JS_LOGFILE;
