#import <Foundation/Foundation.h>

@interface Dock : NSObject {
    NSString *badge;
}
- (void) setBadge:(NSString*)value;
- (NSString *) badge;

@property (readwrite, copy) NSString *badge;

@end
