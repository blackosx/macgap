//
//  AppDelegate.m
//  MacGap
//
//  Created by Alex MacCaw on 08/01/2012.
//  Copyright (c) 2012 Twitter. All rights reserved.
//
//  Edited by Blackosx - October 2014
//
#import "AppDelegate.h"

@implementation AppDelegate

@synthesize windowController;

- (void) applicationWillFinishLaunching:(NSNotification *)aNotification
{

}

-(BOOL)applicationShouldHandleReopen:(NSApplication*)application
                   hasVisibleWindows:(BOOL)visibleWindows{
    if(!visibleWindows){
        [self.windowController.window makeKeyAndOrderFront: nil];
    }
    return YES;
}

- (void) applicationDidFinishLaunching:(NSNotification *)aNotification {
    self.windowController = [[WindowController alloc] initWithURL: kStartPage];
    [self.windowController showWindow: [NSApplication sharedApplication].delegate];
    self.windowController.contentView.webView.alphaValue = 1.0;
    self.windowController.contentView.alphaValue = 1.0;
    [self.windowController showWindow:self];
    
    NSString *TMP_APP_DIR = [NSString stringWithFormat:@"%@/%@", TMP_DIR, MY_APP_NAME];
    NSString *BASH_TO_JS_LOGFILE = [NSString stringWithFormat:@"%@/%@/%@", TMP_DIR, MY_APP_NAME, BASH_TO_JS_FILENAME];
    NSString *JS_TO_BASH_LOGFILE = [NSString stringWithFormat:@"%@/%@/%@", TMP_DIR, MY_APP_NAME, JS_TO_BASH_FILENAME];
    NSLog (@"App Name: %@", MY_APP_NAME);
    NSLog (@"Bash To JS Log file: %@", BASH_TO_JS_LOGFILE);
    NSLog (@"JS To Bash Log file: %@", JS_TO_BASH_LOGFILE);
    NSLog (@"App temp dir: %@", TMP_APP_DIR);
    
    // blackosx added to run bash script on launch
    NSTask *task = [[NSTask alloc] init];
    NSString *taskPath =
    [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] resourcePath], @"public/bash/script.sh"];
    [task setLaunchPath: taskPath];
    [task launch];
    //[task waitUntilExit];
    
    // blackosx add redirect the nslog output to file instead of console
    // Create a directory in tmp
    // http://www.techotopia.com/index.php/Working_with_Directories_in_Objective-C
    NSFileManager *filemgr;
    filemgr = [NSFileManager defaultManager];
    NSURL *newDir = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@", TMP_APP_DIR]];
    [filemgr createDirectoryAtURL: newDir withIntermediateDirectories:YES attributes: nil error:nil];
    
    // Changing to a Different Directory
    NSString *currentpath;
    filemgr = [NSFileManager defaultManager];
    currentpath = [filemgr currentDirectoryPath];
    NSLog (@"Current directory is %@", currentpath);
    if ([filemgr changeCurrentDirectoryPath: [NSString stringWithFormat:@"%@", TMP_APP_DIR]] == NO)
        NSLog (@"Cannot change directory.");
    currentpath = [filemgr currentDirectoryPath];
    NSLog (@"Current directory is %@", currentpath);
    
    // Set log path
    // http://stackoverflow.com/questions/3184235/how-to-redirect-the-nslog-output-to-file-instead-of-console
    NSString *logPath = [NSString stringWithFormat:@"%@", JS_TO_BASH_LOGFILE];
    freopen([logPath fileSystemRepresentation],"a+",stderr);
    
    // create file to act as log for bash to send messages to javascript
    [[NSFileManager defaultManager] createFileAtPath:[NSString stringWithFormat:@"%@", BASH_TO_JS_LOGFILE] contents:nil attributes:nil];
    
    // test writing to file
    //NSString *str = @"1@Test\n2@Arse\n3@Box\n4@Car\n5@Raver\n6@Chicken\n7@Random\n8@Key\n9@Otter\n";
    //[str writeToFile:[NSString stringWithFormat:@"%@", BASH_TO_JS_LOGFILE] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
}

// blackosx added to quit application on window close.
// http://stackoverflow.com/questions/14449986/quitting-an-app-using-the-red-x-button
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}

- (NSString *)pathForTemporaryFileWithPrefix:(NSString *)prefix
{
    NSString *  result;
    CFUUIDRef   uuid;
    CFStringRef uuidStr;
    
    uuid = CFUUIDCreate(NULL);
    assert(uuid != NULL);
    
    uuidStr = CFUUIDCreateString(NULL, uuid);
    assert(uuidStr != NULL);
    
    result = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@", prefix, uuidStr]];
    assert(result != nil);
    
    CFRelease(uuidStr);
    CFRelease(uuid);
    
    return result;
}

// Blackosx added to respond to menu item action to open log file in Finder
// ref: http://stackoverflow.com/questions/15842226/how-to-enable-main-menu-item-copy
- (IBAction)openLog:(id)sender; {
    //NSString * path    = @"/tmp/MY_APP_NAME/MY_APP_NAME_Log.txt";
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@_Log.txt", TMP_DIR, MY_APP_NAME, MY_APP_NAME];
    NSURL    * fileURL = [NSURL fileURLWithPath: path];
    NSWorkspace * ws = [NSWorkspace sharedWorkspace];
    [ws openURL: fileURL];
}

@end